# Logger.js

A logging library for Javascript, inspired by monolog.




## Introduction

The main class, `Logger`, provides an interface for writing log messages to
various targets. Each Logger instance takes a channel name as an argument upon instanciation.
There are several methods for logging available, but by themselves they
do not do anything. For an actual log to be recorded, a stream handler has to
be added to the logger instance via the `pushHandler` method.
Different log levels are supported for different stream handlers.
See **Log Levels** below.

While this library provides some of its own stream handlers, it is designed
to be flexible and expandable, which means you can easily write your own
code to handle streams. See **Custom Stream Handlers** below.


### Basic Example

	let logger = new Logger("main"); // this instance will log to channel 'main'

	// Set up logging to the javascript console, using the default
	// format string. Only messages with loglevel LOGGER_ERROR or
	// higher will be logged by this stream handler.

	logger.pushHandler(new ConsoleLogger(), LOGGER_ERROR);


	// Actually log an error

	logger.error(
		"An error occured", // The log message
		{"errorCode": 101}  // optional, additional data
	);




## Stream Handlers

Stream handlers, as the name suggests, handle streams originating from the
Logger instance.

### Built-in Stream Handlers

#### ConsoleLogger (format)

Logs to the Javascript console.

`format` is a string specifying what the log
line should look like (see **Log Formatting** below). If left undefined, the
default format will be used.


#### MemoryLogger (format)

Stores the logs as a string in `MemoryLogger.logs`, so they can for example be
sent to a server to provide debug information when an error in the client
application occurs.

`format` is a string specifying what the log line should
look like (see **Log Formatting** below). If left undefined, the default format
will be used.


### Custom Stream Handlers

You can write a custom stream handler object. All it needs to implement is a
method named `handleStream`, which takes in one argument, via which the log
entry will be passed to the method. You can then add an instance of that class
as a stream handler via the `Logger.pushHandler` method.

The log entry that will be passed to the `handleStream` method looks like this:

* **entry.channel**: The channel to log the message to as a string
* **entry.timestamp**: Unix timestamp of the log entry (seconds since Epoch)
* **entry.message**: The log message
* **entry.additionalData**: Any additional (debug) data for the log entry
* **entry.loglevel**: the log level of the entry
* **entry.loglevelName**: A string with the human readable log level

Your stream handler can handle the data it receives however you see fit.




## Log Levels

This library implements 7 log levels, referenced by the global constants listed
below:

	const LOGGER_DEBUG     = 0;
	const LOGGER_INFO      = 1;
	const LOGGER_WARNING   = 2;
	const LOGGER_ERROR     = 3;
	const LOGGER_CRITICAL  = 4;
	const LOGGER_ALERT     = 5;
	const LOGGER_EMERGENCY = 6;




## Log Formatting

The `formatLog (format, entry)` function provides an easy way to format log
entries and can be used by your custom stream handlers if you like. `format` is
a string specifying what the log line will look like, and `entry` is an object
containing the log data. See the section **Log Entries** under **Custom Stream
Handlers** above. `formatLog` will return the formatted log line as a string.
The following character combinations have special meaning and will be replaced
by the respective values listed below:

	%Y the full year
	%m the month as a two digit decimal number (01-12)
	%d the day of the month as a two digit decimal number (01-31)

	%H the hour of the day as a two digit decimal number (00-23)
	%M the minute of the hour as a two digit decimal number (00-59)
	%S the second of the minute as a two digit decimal number (00-59)

	%c the log channel
	%l the loglevel
	%s the message string to log
	%a the additional data as JSON

All built-in stream handlers use this function and the above format specifiers
for log formatting.




## The Logger class

The Logger class implements the following methods:


### pushHandler (callback, minimumLoglevel)

Registers the object `callback` (see **Custom Stream Handlers** above) to handle
log events of `minimumLoglevel` and higher. See **Log Levels** above for
available constants to use.

Stream handlers are called in the order they were
registered.


### log (loglevel, message, additionalData)

Log a `message` with log level `loglevel` using the registered stream handlers.
`additionalData` can be any useful context information like variables or arrays
that make the log entry more useful.


#### Wrappers for log
There are several wrapper methods for `Logger.log` in the `Logger` class, making
it quicker to log messages of various log levels.

* debug (message, additionalData)
* info (message, additionalData)
* warning (message, additionalData)
* error (message, additionalData)
* critical (message, additionalData)
* alert (message, additionalData)
* emergency (message, additionalData)




## ToDos

* Use a more sophisticated algorithm of replacing special character combinations
  with data in `formatLog` (currently using `string.replace`)
* Implement a stream handler that posts log entries as JSON to a user specified
API endpoint
