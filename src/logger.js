

// logger.js
// Copyright 2021 Laurenz Wessely
// MIT License




const LOGGER_DEBUG     = 0;
const LOGGER_INFO      = 1;
const LOGGER_WARNING   = 2;
const LOGGER_ERROR     = 3;
const LOGGER_CRITICAL  = 4;
const LOGGER_ALERT     = 5;
const LOGGER_EMERGENCY = 6;




function formatLog (format, entry)
{
	// FORMAT
	// ------
	// Implements the following character conversions, similar to strftime:
	//
	//   %Y the full year
	//   %m the month as a two digit decimal number (01-12)
	//   %d the day of the month as a two digit decimal number (01-31)
	//
	//   %H the hour of the day as a two digit decimal number (00-23)
	//   %M the minute of the hour as a two digit decimal number (00-59)
	//   %S the second of the minute as a two digit decimal number (00-59)
	//
	// And the following character converseions different from strftime:
	//
	//   %c the log channel
	//   %l the loglevel
	//   %s the message string to log
	//   %a the additional data as JSON
	//
	// Note that the additional data wil always be logged to the console
	// in a native format after the log entry.


	if (format === undefined)
		format = "[%Y-%m-%d %H:%M:%S] %c.%l: %s // %a";


	function lpadString (string, fillChar, length)
	{
		string   = string + "";
		fillChar = fillChar + "";

		while (string.length < length)
		{
			string = fillChar + string;
		}


		return string;
	}


	let d = new Date(entry.timestamp * 1000);

	let month  = lpadString(d.getMonth(), 0, 2);
	let day    = lpadString(d.getDay(), 0, 2);
	let hour   = lpadString(d.getHours(), 0, 2);
	let minute = lpadString(d.getMinutes(), 0, 2);
	let second = lpadString(d.getSeconds(), 0, 2);


	// This needs to be improved:
	let logline = format
		.replace(/%Y/g, d.getFullYear())
		.replace(/%m/g, month)
		.replace(/%d/g, day)
		.replace(/%H/g, hour)
		.replace(/%M/g, minute)
		.replace(/%S/g, second)
		.replace(/%c/g, entry.channel)
		.replace(/%l/g, entry.loglevelName)
		.replace(/%s/g, entry.message)
		.replace(/%a/g, JSON.stringify(entry.additionalData));


	return logline;

}




function ConsoleLogger (format)
{
	this.handleStream = function (entry)
	{
		console.log(formatLog(format, entry));
		console.log(entry.additionalData);
	}

}




function MemoryLogger (format)
{
	let that = this;


	this.logs = "";


	this.handleStream = function (entry)
	{
		that.logs += formatLog(format, entry) + "\n";
	}
}




function Logger (channel)
{
	let that = this;


	this.channel  = channel;
	this.handlers = [];




	this.pushHandler = function (callback, minimumLoglevel)
	{
		let handler = []

		handler.callback        = callback;
		handler.minimumLoglevel = minimumLoglevel;

		that.handlers.push(handler);
	}




	this.log = function (loglevel, message, additionalData)
	{
		let loglevelNames = [
			"DEBUG",
			"INFO",
			"WARNING",
			"ERROR",
			"CRITICAL",
			"ALERT",
			"EMERGENCY"
		];


		let entry = [];

		entry.channel        = that.channel;
		entry.timestamp      = Math.round((new Date()).getTime() / 1000);
		entry.message        = message;
		entry.additionalData = additionalData;
		entry.loglevel       = loglevel;
		entry.loglevelName   = loglevelNames[loglevel];


		that.handlers.forEach(
			handler => {

				if (handler.minimumLoglevel <= loglevel)
					handler.callback.handleStream(entry);

			}
		);
	}




	this.debug = function (message, additionalData)
	{
		that.log(LOGGER_DEBUG, message, additionalData);
	}




	this.info = function (message, additionalData)
	{
		that.log(LOGGER_INFO, message, additionalData);
	}




	this.warning = function (message, additionalData)
	{
		that.log(LOGGER_WARNING, message, additionalData);
	}




	this.error = function (message, additionalData)
	{
		that.log(LOGGER_ERROR, message, additionalData);
	}




	this.critical = function (message, additionalData)
	{
		that.log(LOGGER_CRITICAL, message, additionalData);
	}




	this.alert = function (message, additionalData)
	{
		that.log(LOGGER_ALERT, message, additionalData);
	}




	this.emergency = function (message, additionalData)
	{
		that.log(LOGGER_EMERGENCY, message, additionalData);
	}
}








if (typeof(module) !== undefined) {

	module.exports = {

		Logger,
		formatLog,
		ConsoleLogger,
		MemoryLogger,

		loglevels: {
			LOGGER_DEBUG,
			LOGGER_INFO,
			LOGGER_WARNING,
			LOGGER_ERROR,
			LOGGER_CRITICAL,
			LOGGER_ALERT,
			LOGGER_EMERGENCY
		}

	}

}
